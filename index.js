#!/usr/bin/env node

'use strict'

const fs = require('fs')
const path = require('path')
const merge = require('deepmerge')
const program = require('commander')

const parseList = (list) => {
  return list.split(',')
}

const extension = (file) => {
  return path.extname(file) === '.json'
}

const mergeFiles = (options) => {
  let files = options.files || []
  let directory = options.dir || false
  if (files.length === 0 && directory === false) {
    program.help()
  }
  const outputFile = options.out
  let objects = []
  try {
    if (directory) {
      directory = path.resolve(directory)
      if (fs.existsSync(directory)) {
        const list = fs.readdirSync(directory)
        list.filter(extension).forEach((file) => {
          files.push(path.resolve(directory, file))
        })
      } else {
        console.error('Directory does not exist')
      }
    }
    for (let file of files) {
      if (file.indexOf('.json') > 0) {
        console.debug('Loading file', file)
        if (fs.existsSync(file) === false) {
          console.log('File does not exist', file)
          return false
        }
        const data = fs.readFileSync(file)
        console.debug('Parsing json', file)
        const json = JSON.parse(data)
        objects.push(json)
      } else {
        console.debug('Skipping ' + file + ' - only json files are supported!')
      }
    }
    console.debug('Merging data from ' + objects.length + ' json objects')
    const combined = merge.all(objects)
    console.log('Writing merged json file to', outputFile)
    const data = JSON.stringify(combined, null, 4)
    fs.writeFileSync(outputFile, data)
  } catch (error) {
    console.error('Error:', error.message)
  }
}

program
  .version('1.0.0')
  .command('merge')
  .description('Merge multiple JSON files')
  .option('-f, --files <files>', 'List of json files to merge', parseList)
  .option('-d, --dir <directory>', 'Input directory with json files to merge')
  .option('-o, --out <filename>', 'Output destination file for merged file', 'combined.json')
  .usage('--files file1.json,file2.json --out combined.json')
  .action(mergeFiles)

process.argv.splice(2, 0, 'merge')

program.parse(process.argv)
